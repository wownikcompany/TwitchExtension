module.exports = {
    presets: [
        `babel-preset-mobx`,
        `@babel/preset-env`,
        `@babel/preset-react`,
        `@babel/preset-flow`,
        `@babel/preset-typescript`
    ],
    plugins: [
        [`@babel/plugin-proposal-decorators`, { legacy: true }],
        [`@babel/plugin-proposal-class-properties`, { loose: true }],
        `@babel/plugin-transform-runtime`,
        `@babel/transform-arrow-functions`,
        `@babel/plugin-proposal-export-default-from`,
        `@babel/plugin-syntax-dynamic-import`,
        `@babel/plugin-proposal-optional-chaining`,
        [`babel-plugin-wildcard`, {
            exts: [`js`, `svg`],
            useCamelCase: true
        }],
        `@babel/plugin-proposal-object-rest-spread`
    ]
};
